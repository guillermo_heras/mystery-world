﻿using UnityEngine;
using TinkerWorX.AccidentalNoiseLibrary;

public class Generator : MonoBehaviour {

    [SerializeField]
    int Width = 256;
    [SerializeField]
    int Height = 256;
    [SerializeField]
    int TerrainOctaves = 6;
    [SerializeField]
    double TerrainFrequency = 1.25;
    [SerializeField]
    float DeepWater = 0.2f;
    [SerializeField]
    float ShallowWater = 0.4f;
    [SerializeField]
    float Sand = 0.5f;
    [SerializeField]
    float Grass = 0.7f;
    [SerializeField]
    float Forest = 0.8f;
    [SerializeField]
    float Rock = 0.9f;
    [SerializeField]
    float Snow = 1;
    [SerializeField]
    FractalType fractalType = FractalType.Multi;
    [SerializeField]
    BasisType basisType = BasisType.Simplex;
    [SerializeField]
    InterpolationType interpolationType = InterpolationType.Quintic;

    //Noise generator module
    ImplicitFractal HeightMap;

    //Height map data
    MapData HeightData;

    //Final objects
    Tile[,] Tiles;

    //Our texture output
    MeshRenderer HeightMapRenderer;

    void Start()
    {
        HeightMapRenderer = transform.Find("HeightTexture").GetComponent<MeshRenderer>();

        Initialize();

        GetData(HeightMap, ref HeightData);

        LoadTiles();

        UpdateNeighbors();
        UpdateBitmasks();

        HeightMapRenderer.materials[0].mainTexture = TextureGenerator.GetTexture(Width, Height, Tiles);
    }

    private void Initialize()
    {
        HeightMap = new ImplicitFractal(fractalType,basisType,interpolationType);
        HeightMap.Frequency = TerrainFrequency;
        HeightMap.Octaves = TerrainOctaves;
        HeightMap.Seed= UnityEngine.Random.Range(0, int.MaxValue);
        HeightMap.ResetAllSources();
    }

    private void GetData(ImplicitModuleBase module, ref MapData mapData)
    {
        mapData = new MapData(Width, Height);

        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                //Noise range
                float x1 = 0, x2 = 1;
                float y1 = 0, y2 = 1;
                float dx = x2 - x1;
                float dy = y2 - y1;

                float s = x / (float)Width;
                float t = y / (float)Height;

                float nx = x1 + Mathf.Cos(s * 2 * Mathf.PI) * dx / (2 * Mathf.PI);
                float ny = y1 + Mathf.Cos(t * 2 * Mathf.PI) * dy / (2 * Mathf.PI);
                float nz = x1 + Mathf.Sin(s * 2 * Mathf.PI) * dx / (2 * Mathf.PI);
                float nw = y1 + Mathf.Sin(t * 2 * Mathf.PI) * dy / (2 * Mathf.PI);

                float heightValue = (float)HeightMap.Get(nx, ny, nz, nw);

                if (heightValue > mapData.Max)
                    mapData.Max = heightValue;
                if (heightValue < mapData.Min)
                    mapData.Min = heightValue;

                mapData.Data[x, y] = heightValue;
            }
        }
    }

    private void LoadTiles()
    {
        Tiles = new Tile[Width, Height];
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                Tile t = new Tile();
                t.X = x;
                t.Y = y;

                float value = HeightData.Data[x, y];

                //normalize
                value = (value - HeightData.Min) / (HeightData.Max - HeightData.Min);

                t.HeightValue = value;

                //HeightMap Analyze
                if (value < DeepWater)
                {
                    t.HeightType = HeightType.DeepWater;
                }
                else if (value < ShallowWater)
                {
                    t.HeightType = HeightType.ShallowWater;
                }
                else if (value < Sand)
                {
                    t.HeightType = HeightType.Sand;
                }
                else if (value < Grass)
                {
                    t.HeightType = HeightType.Grass;
                }
                else if (value < Forest)
                {
                    t.HeightType = HeightType.Forest;
                }
                else if (value < Rock)
                {
                    t.HeightType = HeightType.Rock;
                }
                else
                {
                    t.HeightType = HeightType.Snow;
                }

                Tiles[x, y] = t;
            }
        }
    }

    private Tile GetTop(Tile t)
    {
        return Tiles[t.X, MathHelper.Mod(t.Y - 1, Height)];
    }
    private Tile GetBottom(Tile t)
    {
        return Tiles[t.X, MathHelper.Mod(t.Y + 1, Height)];
    }
    private Tile GetLeft(Tile t)
    {
        return Tiles[MathHelper.Mod(t.X - 1, Width), t.Y];
    }
    private Tile GetRight(Tile t)
    {
        return Tiles[MathHelper.Mod(t.X + 1, Width), t.Y];
    }

    private void UpdateNeighbors()
    {
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                Tile t = Tiles[x, y];

                t.Top = GetTop(t);
                t.Bottom = GetBottom(t);
                t.Left = GetLeft(t);
                t.Right = GetRight(t);
            }
        }
    }

    private void UpdateBitmasks()
    {
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                Tiles[x, y].UpdateBitmask();
            }
        }
    }
}
